/*
Memory read and write operation
*/
#define WIN32_LEAN_AND_MEAN
#include <stdbool.h>		//bool stuff
#include <stdlib.h>			//use std lib
#include <stdio.h>			//use std IO
#include <string.h>
#include <ctype.h>
#include <fcntl.h>
//#include <io.h>				//lo level file

#include "list.h"			//list may be used
#include "memory.h"			//memory prototypes


typedef struct
{
	unsigned dump:1;
}MEM_CONFIG;



LIST_ITEM *Memory_List = NULL;

MEM_CONFIG Memory_Config;	//allocated across all memory

bool mode_mem = true;


//***************************************************************
//Description:	ASCII Dump of memory one line
//Arguments:	Logical Address
//				Pointer to a large string array
//Returns:		Next Logical Address
//****************************************************************
unsigned int Memory_Dump32(MEM_STRUCT *ms,unsigned int start,char *sptr)
{
	unsigned int retval = 0;
	unsigned char *vp = 0;
	int count;
	unsigned char umsb;
	unsigned char ulsb;
	unsigned char lmsb;
	char adata[16];

	if((sptr != NULL) && (ms != NULL))
	{
		if(start < ms->size)
		{
			vp = (unsigned char *)ms->mptr + start;
			sprintf(sptr,"%8.8X: ",start);
			count = 4;
			while(count)
			{
				umsb = *vp; vp++;
				ulsb = *vp; vp++;
				lmsb = *vp; vp++;
				if(Memory_Config.dump == LSB)
				{
					sprintf(adata,"%2.2X%2.2X%2.2X%2.2X ",(unsigned short) *vp,lmsb,ulsb,umsb);
				}
				else
				{
					sprintf(adata,"%2.2X%2.2X%2.2X%2.2X ",umsb,ulsb,lmsb,(unsigned short) *vp);
				}
				strcat(sptr,adata);
				vp++;
				count--;
			}
			retval = strlen(sptr);
		}
	}
	return(retval);
}



//***************************************************************
//Description:	ASCII Dump of memory one line
//Arguments:	Logical Address
//				Pointer to a large string array
//Returns:		Next Logical Address
//****************************************************************
unsigned int Memory_Dump16(MEM_STRUCT *ms,unsigned int start,char *sptr)
{
	unsigned int retval = 0;
	unsigned char *vp = 0;
	int count;
	unsigned char data;
	char adata[16];

	if((sptr != NULL) && (ms != NULL))
	{
		if(start < ms->size)
		{
			vp = (unsigned char *)ms->mptr + start;
			sprintf(sptr,"%8.8X: ",start);
			count = 8;
			while(count)
			{
				data = *vp;
				vp++;
				if(Memory_Config.dump == LSB)
				{
					sprintf(adata,"%2.2X%2.2X ",(unsigned short) *vp,data);
				}
				else
				{
					sprintf(adata,"%2.2X%2.2X ",data,(unsigned short) *vp);
				}
				strcat(sptr,adata);
				vp++;
				count--;
			}
			retval = strlen(sptr);
		}
	}
	return(retval);
}



//***************************************************************
//Description:	ASCII Dump of memory one line
//Arguments:	Logical Address
//				Pointer to a large string array
//Returns:		Next Logical Address
//****************************************************************
unsigned int Memory_Dump(MEM_STRUCT *ms,unsigned int start,char *sptr)
{
	unsigned int retval = 0;
	unsigned char *vp = 0;
	unsigned char *ap = 0;
	int count;
//	unsigned char data;
	char adata[16];

	if((sptr != NULL) && (ms != NULL))
	{
		if(start < ms->size)
		{
			vp = (unsigned char *)ms->mptr + start;
			ap = vp;
			sprintf(sptr,"%8.8X: ",start);
			count = 16;
			while(count)
			{
				sprintf(adata,"%2.2X ",(unsigned char) *vp);
				strcat(sptr,adata);
				vp++;
				count--;
			}
			count = 16;
			while(count)
			{
				if(isalnum(*ap))
				{
					sprintf(adata,"%c",(unsigned char) *ap);
					strcat(sptr,adata);
				}
				else
				{
					strcat(sptr,".");
				}
				ap++;
				count--;
			}
			retval = strlen(sptr);
		}
	}
	return(retval);
}





//**************************************************************
//Description:	move file into memory contents
//Arguments:	Pointer to file string name
//				start location in memory
//Return:		True file opened and closed ok
//				false bad somthing
//**************************************************************
int Memory_ReadFile(MEM_STRUCT *ms,char *filename,unsigned int start)
{
	FILE *file = NULL;
	int retval = -1;
	unsigned long fileLen;


	if((ms != NULL) && (filename != NULL))
	{
		if((file = fopen(filename, "rb")) == NULL)
		{
			fprintf(stderr, "can't open file %s", filename);
			exit(1);
		}
		fseek(file, 0, SEEK_END);
		fileLen=ftell(file);
		fseek(file, 0, SEEK_SET);

		retval = fread(ms->mptr, 1, ms->size, file);

	}
	fclose(file);
	return(retval);
}







//*********************************************************************
//Description:  Memory ReadByte
//Arguments:    Address
//              Pointer to byte
//Return:       True valid memory
//              False
//**********************************************************************
bool Memory_Read8Bits(MEM_STRUCT *ms,unsigned int address,unsigned char *data)
{
	bool retval = false;
	unsigned char *ta;

	if((data != NULL) && (ms != NULL))
	{
		ta = (unsigned char *)ms->mptr + address;
   		*data = *ta;
   		retval = true;
   	}
  	return(retval);
}

//*********************************************************************
//Description:  Memory ReadWord
//Arguments:    Address
//              Pointer to word
//Return:       True valid memory
//              False
//**********************************************************************
bool Memory_Read16Bits(MEM_STRUCT *ms,unsigned int address,unsigned short *data)
{
	bool retval = false;
	unsigned short *ta;

	if((data != NULL) && (ms != NULL))
	{
		ta = (unsigned short *)ms->mptr + address;
		*data = *ta;
   		retval = true;
	}
	return(retval);
}


//*****************

//*********************************************************************
//Description:  Memory ReadLong
//Arguments:    Pointer structure
//				Address
//              Pointer to long
//Return:       True valid memory
//              False
//**********************************************************************
bool Memory_Read32Bits(MEM_STRUCT *ms, unsigned int address, unsigned int *data)
{
	bool retval = false;
	unsigned int ta;
	unsigned char *ca;
	int i;


	if((ms != NULL) && (data != NULL))
	{
		if(address < ms->size)
		{
			ta = 0;
			ca = ms->mptr;
			ca += address;

			if(ms->mode)    //big indians
        	{
				i = 24;
				do
				{
					ta |= *ca << i;
					i = i - 8;
					ca++;
            	}while(i);
        	}
        	else
        	{
				i = 0;
   				do
   				{
      				ta |= *ca << i;
	       			i = i + 8;
       				ca++;
   				}while(i < 32);
   			}
			*data = ta;
			retval = true;
		}
	}

	return(retval);
}



//*********************************************************************
//Description:  Write Byte
//Argumets:     Address
//              data
//Return:       true
//*********************************************************************
bool Memory_Write8Bits(MEM_STRUCT *ms,unsigned int address,unsigned char data)
{
	bool retval = false;
	unsigned char *ta;

	if(ms != NULL)
	{
		ta = (unsigned char *)ms->mptr + address;
		*ta = data;
   		retval = true;
   	}
   	return(retval);
}


//*********************************************************************
//Description:  Write Word
//Argumets:     Address
//              data
//Return:       true
//*********************************************************************
bool Memory_Write16Bits(MEM_STRUCT *ms,unsigned int address,unsigned short data)
{
	bool retval = false;
	unsigned short *ta;

	if(ms != NULL)
	{
		ta = (unsigned short *)ms->mptr + address;
		*ta = data;
		retval = true;
   	}
   	return(retval);
}


//*********************************************************************
//Description:	Write Long
//Argumets:	Address
//			data
//Return:		true
//*********************************************************************
bool Memory_Write32Bits(MEM_STRUCT *ms,unsigned int address,unsigned int data)
{
	bool retval = false;
    unsigned long *ta;

	if(ms != NULL)
	{
		ta = (unsigned long *)ms->mptr + address;
		*ta = data;
		retval = true;
	}
	return(retval);
}



//*************************************************************************
//Description:	Initialise a memory stucture
//Arguments:	Start of memory
//				Size of memory
//Return:		Pointer to structure
//				NULL if failed
//*************************************************************************
MEM_STRUCT *Memory_Init(int start, int size, bool mode)
{
	MEM_STRUCT *retptr = NULL;
	void *mptr = NULL;

	if(size != 0)
	{
		//create the dynamic structure
		retptr = (MEM_STRUCT *)malloc(sizeof(MEM_STRUCT));
		mptr = malloc(size);
		if(retptr != NULL)
		{
			if(mptr != NULL)
			{
				retptr->mptr = mptr;
				retptr->size = size;
				retptr->start = start;
				retptr->mode = mode;
			}
			else
			{
				//error get rid of structure
				free(retptr);
				retptr = NULL;
			}
		}
		else
		{
			free(mptr);
		}
	}

	return(retptr);
}


//***********************************************************
//Description:	Close of the memory structure
//Arguments:	Pointer to memory structre
//Return:		True for sucseeful
//***********************************************************
bool Memory_Close(MEM_STRUCT *ms)
{
	bool retval = false;
	
	if(ms != NULL)
	{
		if(ms->mptr != NULL)
		{
			free(ms->mptr);
			ms->mptr = NULL;	
		}
		
		free(ms);
		ms = NULL;
		retval = true;
	}
	return(retval);	
}
