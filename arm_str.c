//******************************************
//Arm string generator
//Decodes the instructions for arm
//******************************************

#include <stdlib.h>
#include <stdbool.h>
#include "memory.h"
#include "register.h"
#include "arm.h"
#include "arm_str.h"

//structure for opcode decode
typedef struct
{
	unsigned int mask;
	unsigned int opcode;
	char *(*opcodecall)(CPU_STRUCT *);
}CODE_TABLE;



char *ST_SWP(CPU_STRUCT *cpu)
{
	if(cpu != NULL)
	{
		cpu->rb = cpu->rb;
	}
//		unsigned int dwtmp=REG(DECODEDINS.Rm);
//		instclock=4;

///		REG(DECODEDINS.Rd) = *(unsigned int *)(mem+REG(DECODEDINS.Rn));
///        *(unsigned int *)(mem+REG(DECODEDINS.Rn))=dwtmp;

//	if(DECODEDINS.Rd!=15)
//	{
//		PC+=4;
//		REG(15)+=4;
//	}
//	else
//	{
//		REG(15)&=0xfffffffc;
//		PC=REG(15);
//		REG(15)=PC+PC_AHEAD;
//	}
    return("SWP");
}


//opcode lookup table
CODE_TABLE opcodestr[]=
{
	//swap
	{0x0FF00FF0,0x01000090,	&ST_SWP},   //0
	{0x0FF00FF0,0x01400F90,	&ST_SWP},					//&OP_SWPB},
	//state register accessing instructions
	{0x0FF00000,0x01000000,	&ST_SWP},					//&OP_MRS_CPSR},
	{0x0FF00000,0x01400000,	&ST_SWP},					//&OP_MRS_SPSR},
	{0x0FF000F0,0x01200000,	&ST_SWP},	//				//&OP_MSR_CPSR_RM},
	{0x0FF000F0,0x01600000,	&ST_SWP},	//5				//&OP_MSR_SPSR_RM},
	{0x0FF00000,0x03200000,	&ST_SWP},					//&OP_MSR_CPSR_IMM},
	{0x0FF00000,0x03600000,	&ST_SWP},					//&OP_MSR_SPSR_IMM},
	//multiply
	{0x0FE0F0F0,0x00000090,	&ST_SWP},					//&OP_MUL},
	{0x0FE000F0,0x00200090,	&ST_SWP},					//&OP_MLA},
	{0x0FE000F0,0x00800090,	&ST_SWP},	//10			//&OP_UMULL},
	{0x0FE000F0,0x00A00090,	&ST_SWP},					//&OP_UMLAL},
	{0x0FE000F0,0x00C00090,	&ST_SWP},					//&OP_SMULL},
	{0x0FE000F0,0x00E00090,	&ST_SWP},					//&OP_SMLAL},
	//misc
	{0x0FFF0FF0,0x016F0F10,	&ST_SWP},					//&OP_CLZ},
	//ALU instructions
	{0x0DE00000,0x00000000,	&ST_SWP},	//15			//&OP_AND},
	{0x0DE00000,0x00200000,	&ST_SWP},					//&OP_EOR},
	{0x0DE00000,0x00400000,	&ST_SWP},					//&OP_SUB},
	{0x0DE00000,0x00600000,	&ST_SWP},					//&OP_RSB},
	{0x0DE00000,0x00800000,	&ST_SWP},					//&OP_ADD},
	{0x0DE00000,0x00A00000,	&ST_SWP},	//20			//&OP_ADC},
	{0x0DE00000,0x00C00000,	&ST_SWP},					//&OP_SBC},
	{0x0DE00000,0x00E00000,	&ST_SWP},					//&OP_RSC},
	{0x0DE00000,0x01000000,	&ST_SWP},					//&OP_TST},
	{0x0DE00000,0x01200000,	&ST_SWP},					//&OP_TEQ},
	{0x0DE00000,0x01400000,	&ST_SWP},	//25			//&OP_CMP},
	{0x0DE00000,0x01600000,	&ST_SWP},					//&OP_CMN},
	{0x0DE00000,0x01800000, &ST_SWP},               	//&OP_ORR},
	{0x0DE00000,0x01A00000, &ST_SWP},
	{0x0DE00000,0x01C00000,	&ST_SWP},					//&OP_BIC},
	{0x0DE00000,0x01E00000,	&ST_SWP},	//30			//&OP_MVN},
	//LDR/STR w/o write-back w=0,p=1
	{0x0F700000,0x05100000, &ST_SWP},            //31
	{0x0F700000,0x05500000,	&ST_SWP},					//&OP_LDRB_IMM},
	{0x0F700000,0x05000000,	&ST_SWP},					//&OP_STR_IMM},
	{0x0F700000,0x05400000,	&ST_SWP},					//&OP_STRB_IMM},
	{0x0F700FF0,0x07100000,	&ST_SWP},	//35			//&OP_LDR_RM},
	{0x0F700FF0,0x07500000,	&ST_SWP},					//&OP_LDRB_RM},
	{0x0F700FF0,0x07000000,	&ST_SWP},					//&OP_STR_RM},
	{0x0F700FF0,0x07400000,	&ST_SWP},					//&OP_STRB_RM},
	{0x0F700010,0x07100000,	&ST_SWP},					//&OP_LDR_RM_SHIFT},
	{0x0F700010,0x07500000,	&ST_SWP},	//40			//&OP_LDRB_RM_SHIFT},
	{0x0F700010,0x07000000,	&ST_SWP},					//&OP_STR_RM_SHIFT},
	{0x0F700010,0x07400000,	&ST_SWP},					//&OP_STRB_RM_SHIFT},
	//LDR/STR pre-indexed w=1,p=1
	{0x0F700000,0x05300000,	&ST_SWP},					//&OP_LDR_IMM_PRE},
	{0x0F700000,0x05700000,	&ST_SWP},					//&OP_LDRB_IMM_PRE},
	{0x0F700000,0x05200000,	&ST_SWP},	//45			//&OP_STR_IMM_PRE},
	{0x0F700000,0x05600000,	&ST_SWP},					//&OP_STRB_IMM_PRE},
	{0x0F700FF0,0x07300000,	&ST_SWP},					//&OP_LDR_RM_PRE},
	{0x0F700FF0,0x07700000,	&ST_SWP},					//&OP_LDRB_RM_PRE},
	{0x0F700FF0,0x07200000,	&ST_SWP},					//&OP_STR_RM_PRE},
	{0x0F700FF0,0x07600000,	&ST_SWP},	//50			//&OP_STRB_RM_PRE},
	{0x0F700010,0x07300000,	&ST_SWP},					//&OP_LDR_RM_SHIFT_PRE},
	{0x0F700010,0x07700000,	&ST_SWP},					//&OP_LDRB_RM_SHIFT_PRE},
	{0x0F700010,0x07200000,	&ST_SWP},					//&OP_STR_RM_SHIFT_PRE},
	{0x0F700010,0x07600000,	&ST_SWP},					//&OP_STRB_RM_SHIFT_PRE},
	//LDR/STR post-indexed w=0,p=0
	{0x0F700000,0x04100000,	&ST_SWP},	//55			//&OP_LDR_IMM_POST},
	{0x0F700000,0x04500000,	&ST_SWP},					//&OP_LDRB_IMM_POST},
	{0x0F700000,0x04000000,	&ST_SWP},					//&OP_STR_IMM_POST},
	{0x0F700000,0x04400000,	&ST_SWP},					//&OP_STRB_IMM_POST},
	{0x0F700FF0,0x06100000,	&ST_SWP},					//&OP_LDR_RM_POST},
	{0x0F700FF0,0x06500000,	&ST_SWP},	//60			//&OP_LDRB_RM_POST},
	{0x0F700FF0,0x06000000,	&ST_SWP},					//&OP_STR_RM_POST},
	{0x0F700FF0,0x06400000,	&ST_SWP},					//&OP_STRB_RM_POST},
	{0x0F700010,0x06100000,	&ST_SWP},					//&OP_LDR_RM_SHIFT_POST},
	{0x0F700010,0x06500000,	&ST_SWP},					//&OP_LDRB_RM_SHIFT_POST},
	{0x0F700010,0x06000000,	&ST_SWP},	//65			//&OP_STR_RM_SHIFT_POST},
	{0x0F700010,0x06400000,	&ST_SWP},					//&OP_STRB_RM_SHIFT_POST},
	// LDRH/STRH
	{0x0F7000F0,0x015000B0,	&ST_SWP},					//&OP_LDRH_IMM},
	{0x0F7000F0,0x017000B0,	&ST_SWP},					//&OP_LDRH_IMM_PRE},
	{0x0F7000F0,0x005000B0,	&ST_SWP},					//&OP_LDRH_IMM_POST},
	{0x0F7000F0,0x014000B0,	&ST_SWP},	//70    		//&OP_STRH_IMM},
	{0x0F7000F0,0x016000B0,	&ST_SWP},					//&OP_STRH_IMM_PRE},
	{0x0F7000F0,0x004000B0,	&ST_SWP},					//&OP_STRH_IMM_POST},
	{0x0F7000F0,0x011000B0,	&ST_SWP},					//&OP_LDRH_RM},
	{0x0F7000F0,0x013000B0,	&ST_SWP},					//&OP_LDRH_RM_PRE},
	{0x0F7000F0,0x001000B0,	&ST_SWP},	//75			//&OP_LDRH_RM_POST},
	{0x0F7000F0,0x010000B0,	&ST_SWP},					//&OP_STRH_RM},
	{0x0F7000F0,0x012000B0,	&ST_SWP},					//&OP_STRH_RM_PRE},
	{0x0F7000F0,0x000000B0,	&ST_SWP},					//&OP_STRH_RM_POST},
	// LDRSB
	{0x0F7000F0,0x015000D0,	&ST_SWP},					//&OP_LDRSB_IMM},
	{0x0F7000F0,0x017000D0,	&ST_SWP},	//80			//&OP_LDRSB_IMM_PRE},
	{0x0F7000F0,0x005000D0,	&ST_SWP},					//&OP_LDRSB_IMM_POST},
	{0x0F7000F0,0x011000D0,	&ST_SWP},					//&OP_LDRSB_RM},
	{0x0F7000F0,0x013000D0,	&ST_SWP},					//&OP_LDRSB_RM_PRE},
	{0x0F7000F0,0x001000D0,	&ST_SWP},					//&OP_LDRSB_RM_POST},
	// LDRSH
	{0x0F7000F0,0x015000F0,	&ST_SWP},	//85			//&OP_LDRSH_IMM},
	{0x0F7000F0,0x017000F0,	&ST_SWP},					//&OP_LDRSH_IMM_PRE},
	{0x0F7000F0,0x005000F0,	&ST_SWP},					//&OP_LDRSH_IMM_POST},
	{0x0F7000F0,0x011000F0,	&ST_SWP},					//&OP_LDRSH_RM},
	{0x0F7000F0,0x013000F0,	&ST_SWP},					//&OP_LDRSH_RM_PRE},
	{0x0F7000F0,0x001000F0,	&ST_SWP},	//90			//&OP_LDRSH_RM_POST},
	//LDM
	{0x0FF00000,0x08900000,	&ST_SWP},					//&OP_LDM_INC_POST},
	{0x0FF00000,0x08100000,	&ST_SWP},					//&OP_LDM_DEC_POST},
	{0x0FF00000,0x09900000,	&ST_SWP},					//&OP_LDM_INC_PRE},
	{0x0FF00000,0x09100000,	&ST_SWP},					//&OP_LDM_DEC_PRE},
	{0x0FF00000,0x08B00000,	&ST_SWP},	//95			//&OP_LDM_INC_POST_WB},
	{0x0FF00000,0x08300000,	&ST_SWP},					//&OP_LDM_DEC_POST_WB},
	{0x0FF00000,0x09B00000,	&ST_SWP},					//&OP_LDM_INC_PRE_WB},
	{0x0FF00000,0x09300000,	&ST_SWP},					//&OP_LDM_DEC_PRE_WB},
	{0x0FF00000,0x08D00000,	&ST_SWP},					//&OP_LDM_INC_POST_S},
	{0x0FF00000,0x08500000,	&ST_SWP},	//100			//&OP_LDM_DEC_POST_S},
	{0x0FF00000,0x09D00000,	&ST_SWP},					//&OP_LDM_INC_PRE_S},
	{0x0FF00000,0x09500000,	&ST_SWP},					//&OP_LDM_DEC_PRE_S},
	//STM
	{0x0FF00000,0x08800000,	&ST_SWP},					//&OP_STM_INC_POST},
	{0x0FF00000,0x08000000,	&ST_SWP},					//&OP_STM_DEC_POST},
	{0x0FF00000,0x09800000,	&ST_SWP},	//105   		//&OP_STM_INC_PRE},
	{0x0FF00000,0x09000000,	&ST_SWP},					//&OP_STM_DEC_PRE},
	{0x0FF00000,0x08A00000,	&ST_SWP},					//&OP_STM_INC_POST_WB},
	{0x0FF00000,0x08200000,	&ST_SWP},					//&OP_STM_DEC_POST_WB},
	{0x0FF00000,0x09A00000,	&ST_SWP},					//&OP_STM_INC_PRE_WB},
	{0x0FF00000,0x09200000,	&ST_SWP},	//110			//&OP_STM_DEC_PRE_WB},
	{0x0FF00000,0x08C00000,	&ST_SWP},					//&OP_STM_INC_POST_S},
	{0x0FF00000,0x08400000,	&ST_SWP},					//&OP_STM_DEC_POST_S},
	{0x0FF00000,0x09C00000,	&ST_SWP},					//&OP_STM_INC_PRE_S},
	{0x0FF00000,0x09400000,	&ST_SWP},					//&OP_STM_DEC_PRE_S},
	//jump
	{0x0FFFFFF0,0x012FFF10,	&ST_SWP},	//115			//&OP_BX},
	{0x0FE00000,0x0FA00000,	&ST_SWP},					//&OP_BLX1},
	{0x0FFFFFF0,0x0FF00030,	&ST_SWP},					//&OP_BLX2},
	{0x0F000000,0x0A000000, &ST_SWP},
	{0x0F000000,0x0B000000, &ST_SWP},	//branch with link
	//special
	{0x0F000000,0x0F000000,	&ST_SWP},	//120			//&OP_SWI},
  //{0xFF000F0,	0x1200070,	&OP_BKPT},
	{0x0E000010,0x06000010,	&ST_SWP},					//&OP_UNDEF},
	{0,0,					&ST_SWP},					//&OP_NOP}
};



//*****************************************************
//Description:  Decode ARM code turn it into a string
//Argumentes:   code to decode
//              string pointer
//              size of string
//return:       true if good
//******************************************************
bool ARM_Str_Decode(CPU_STRUCT *cpu,int code, char *sptr, int strsize)
{
	bool retval = false;
	int i;

	if((sptr != NULL) && (cpu != NULL))
	{
		for(i=0; opcodestr[i].mask; i++)
		{
			if((code & opcodestr[i].mask) == opcodestr[i].opcode)
			{
				sptr = opcodestr[i].opcodecall(cpu);
				return(retval);
            }
        }
	}
//	cpu->opcodecall = &OP_NOP;
 	return(retval);
}
