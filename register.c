/*
Register Handles the register banks
*/

#include <stdlib.h>
#include <stdbool.h>

#include "register.h"





//****************************************************************
//Description:	Emulate the ARM Barrel shift
//Arguments:    Register Struct
//              index to register to operate on
//              shift code
//Return:       shift results
//****************************************************************
int REG_Barrel(REG_STRUCT *rb, int index, int shift)
{
	int retval;
	int st;
	int sr;
	int sa;


    retval = REG_Read(rb,index);

	if(rb != NULL)
	{
		st = (shift >> 5) & 0x03;	//extract the shift type
		sa = (shift >> 7) & 0x1F;	//extract shift ammount
		sr = (shift >> 8) & 0x0F;	//extract shift register

		if((shift & 0x01) && 0x01)
		{
			switch(st)
			{
				case BL_LSL:	//Logical shift Left
					while(sa)
					{
						REG_C_Write(rb,retval >> 31);
						retval <<= 1;
						sa--;
					}
					break;

				case BL_LSR:	//Logical shift Right
					while(sa)
					{
						REG_C_Write(rb,retval & 0x01);
						retval >>= 1;
						sa--;
					}
					break;

				case BL_ASR:	//Arithmatic shift right
					while(sa)
					{
						sr = retval & 0x80000000;
						REG_C_Write(rb,retval & 0x01);
						retval >>= 1;
						retval |= sr;
						sa--;
					}
					break;

				case BL_ROR:	//Rotate Right
					while(sa)
					{
						sr = retval << 31;
						REG_C_Write(rb,retval & 0x01);
						retval >>= 1;
						retval |= sr;
						sa--;
					}
					break;

				default:		//error
					break;
			}
		}
		else
		{
			switch(st)
			{
				case BL_LSL:	//Logical shift Left
					while(sa)
					{
						REG_C_Write(rb,retval >> 31);
						retval <<= 1;
						sa--;
					}
					break;

				case BL_LSR:	//Logical shift Right
					while(sa)
					{
						REG_C_Write(rb,retval & 0x01);
						retval >>= 1;
						sa--;
					}
					break;

				case BL_ASR:	//Arithmatic shift right
					while(sa)
					{
						sr = retval & 0x80000000;
						REG_C_Write(rb,retval & 0x01);
						retval >>= 1;
						retval |= sr;
						sa--;
					}
					break;

				case BL_ROR:	//Rotate Right
					while(sa)
					{
						sr = retval << 31;
						REG_C_Write(rb,retval & 0x01);
						retval >>= 1;
						retval |= sr;
						sa--;
					}
					break;

				default:		//error
					break;
			}
		}
	}

	return(retval);
}


//**************************************************************************
//Description:	Clear everything in the PSR
//Arguments:	Pointer Register structure
//Retrun:		True
//				False
//*************************************************************************
bool REG_Clr_PSR(REG_STRUCT *r)
{
	bool retval = false;

	if(r != NULL)
	{
		r->cpsr.st = 0;
		retval = true;
	}
	return(retval);
}



//**************************************************************************
//Description:	Get the current mode from the psr
//Arguments:	Pointer to structure
//Return:		mode information
//**************************************************************************
int REG_Read_Mode(REG_STRUCT *r)
{
	int retval = 0;

	if(r != NULL)
	{
		retval = r->cpsr.cs.mode;
	}
	return(retval);
}

//**************************************************************************
//Description:	Read the status of flag Z in program status register
//Arguments:	Pointer to register structure
//Return:		True for set
//			false for clear
//***************************************************************************
bool REG_Z_Status(REG_STRUCT *r)
{
	bool retval = false;

	if(r != NULL)
	{
		retval = r->cpsr.cs.Z;
	}

	return(retval);
}


//************************************************************************
//Description:	Read the status of the carry flag
//Arguments:	pointer to Register structure
//Return:		C status
//************************************************************************
bool REG_C_Status(REG_STRUCT *r)
{
	bool retval = false;

	if(r != NULL)
	{
		retval = r->cpsr.cs.C;
	}
	return(retval);
}

//************************************************************************
//Description:	write carry flag
//Arguments:	pointer to Register structure
//				flag
//************************************************************************
bool REG_C_Write(REG_STRUCT *r, bool f)
{
	bool retval = false;

	if(r != NULL)
	{
		r->cpsr.cs.C = f;
	}
	return(retval);
}

//*************************************************************************
//Description:	Read Status of N flag
//Arguments:	Pointer to register structure
//Return:		N state
//**************************************************************************
bool REG_N_Status(REG_STRUCT *r)
{
	bool retval = false;

	if(r != NULL)
	{
		retval = r->cpsr.cs.N;
	}
	return(retval);
}

//*************************************************************************
//Description:	Read Status of V flag
//Arguments:	Pointer to register structure
//Return:		V state
//**************************************************************************
bool REG_V_Status(REG_STRUCT *r)
{
	bool retval = false;

	if(r != NULL)
	{
		retval = r->cpsr.cs.V;
	}
	return(retval);
}

//***************************************************************************
//Description:	Read status of C and Z to give a state HI
//Arguments:	Pointer to register structure
//Return:		HI
//***************************************************************************
bool REG_HI_Status(REG_STRUCT *r)
{
	bool retval = false;

	if(r != NULL)
	{
		if((!REG_C_Status(r)) && (!REG_Z_Status(r)))
		{
			retval = true;
		}
	}
	return(retval);
}

//***************************************************************************
//Description:	Read the status of Z and C flag to determine LS less then same
//Arguements:	Pointer Register Structure
//Return:		LS
//*****************************************************************************
bool REG_LS_Status(REG_STRUCT *r)
{
	bool retval = false;

	if(r != NULL)
	{
		if((!REG_C_Status(r)) || (REG_Z_Status(r)))
		{
			retval = true;
		}
	}
	return(retval);
}


//****************************************************************************
//Description:	Greater than or equal status GE
//Arguments:	Pointer to Register Structure
//Return:		GE status
//****************************************************************************
bool REG_GE_Status(REG_STRUCT *r)
{
	bool retval = false;

	if(r != NULL)
	{
		if(REG_N_Status(r) == REG_V_Status(r))
		{
			retval = true;
		}
	}
	return(retval);
}


//**************************************************************************
//Description:	Less than status LT
//Arguments:	Pointer to register structure
//Return:		LT
//***************************************************************************
bool REG_LT_Status(REG_STRUCT *r)
{
	bool retval = false;

	if(r != NULL)
	{
		if(REG_N_Status(r) != REG_V_Status(r))
		{
			retval = false;
		}
	}
	return(retval);
}


//*************************************************************************
//Description:	Greater than status GT
//Arguments:	Pointer to reg structure
//Return:		GT
//*************************************************************************
bool REG_GT_Status(REG_STRUCT *r)
{
	bool retval = false;

	if(r != NULL)
	{
		if((!REG_Z_Status(r)) && (REG_GE_Status(r)))
		{
			retval = true;
		}
	}
	return(retval);
}

//*****************************************************************************
//Description:	Less than or equal test LE
//Arguements:	Pointer to structure
//Return:		LE
//*****************************************************************************
bool REG_LE_Status(REG_STRUCT *r)
{
	bool retval = false;

	if(r != NULL)
	{
		if(REG_Z_Status(r) || REG_LT_Status(r))
		{
			retval = true;
		}
	}
	return(retval);
}

//*************************************************************************
//Description:	Read a register
//Arguments:	Pointer to structure
//				register index 0 to 15
//Return:		value
//*************************************************************************
int REG_Read(REG_STRUCT *r, int index)
{
	int retval = 0;

	if(r != NULL)
	{
		if(index < REG_MAX_REG)
		{
			retval = r->rs[index];
		}
	}
	return(retval);
}

//*************************************************************
//Description:	increment the Program Counter useually R15
//			Just add 4 to PC
//Arguments:	Pointer to register structure
//Returns:	True if Register structure is ok
//			False.
//***********************************************************************
bool REG_PC_Inc4(REG_STRUCT *r)
{
	bool retval = false;
	unsigned int rd;
	if(r != NULL)
	{
		rd = r->rs[REG_PC];
		rd += 4;
		r->rs[REG_PC] = rd;
		retval = true;
	}
	return(retval);
}



//*************************************************************
//Description:  Add a value to PC
//Arguements:   Pointer to reg bank structure
//              bank number
//              Value to add
//Return:       PC Value after addition
//************************************************************
int REG_PCAdd(REG_STRUCT *r,int bank,int v)
{
    int retval = 0;

    if((r != NULL) && (bank < REG_MAX_BNK))
    {
        r->rs[REG_PC] += v;
        retval = r->rs[REG_PC];
    }
    return(retval);
}


//************************************************************
//Description:  Read the PC
//Arguments:    Pointer Register Bank
//Returns:      Value of PC
//************************************************************
int REG_Read_pc(REG_STRUCT *r)
{
    int retval = 0;

    if(r != NULL)
    {
        retval = r->rs[REG_PC];
    }
    return(retval);
}


/* REG Write */

//**************************************************************
//Description: Write to an indexed register
//Arguments:	Register structure
//				Register index
//				Data
//Return:		True for good
//				faulse for bad
//***************************************************************
bool REG_Write(REG_STRUCT *r,int index,int d)
{
	bool retval = false;

	if(r != NULL)
	{
		if(index < REG_MAX_REG)
		{
			r->rs[index] = d;
			retval = true;
		}
	}
	return(retval);
}


//*******************************************************************
//Description:	write value to PC
//Arguments:	Pointer to Reg Strcut
//				value to put into PC
//Return:		True if completed
//*****************************************************************
bool REG_Write_pc(REG_STRUCT *r, unsigned int pc)
{
	bool retval = false;

	if(r != NULL)
	{
		r->rs[REG_PC] = pc;
		retval = true;
	}
	return(retval);
}

//*******************************************************************
//Description:	write value to PC
//Arguments:	Pointer to Reg Strcut
//				value to put into PC
//Return:		True if completed
//*****************************************************************
bool REG_Write_Link(REG_STRUCT *r, unsigned int d)
{
	bool retval = false;

	if(r != NULL)
	{
		r->rs[REG_LINKS] = d;
		retval = true;
	}
	return(retval);
}


//********************************************************************
//Description: Reset all register to a known state for the cpu
//Arguments:	Pointer to reg styructure
//Returns:		True completed
//				False somthing wrong
//*********************************************************************
bool REG_Reset(REG_STRUCT *r)
{
	bool retval = false;

	if(r != NULL)
	{
		r->cpsr.st = 0;
		REG_Write_pc(r,0);

	}
	return(retval);
}

//*********************************************************************
//Description:	Initilise the register for CPU
//*********************************************************************
REG_STRUCT * REG_Init(void)
{
	REG_STRUCT *r = NULL;

    r = malloc(sizeof(REG_STRUCT));

    return(r);

}


//*******************************************************************
//Description:	Close of the Register structure
//Argument:		Pointer to structure
//Return:		True if things are good
//******************************************************************
bool REG_Close(REG_STRUCT *r)
{
 	bool retval = false;
  	if(r != NULL)
    {
        free(r);
        retval = true;
    }
    return(retval);

}


