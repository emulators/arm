/*******
Memery Header file
*/
#ifndef MEMORY_H
#define MEMORY_H

#include "list.h"			//list may be used


#define LSB	0
#define MSB	1


typedef struct _MEM_STRUCT
{
	LIST_ITEM list;			//part of list
	unsigned int start;		//start address of memory
	unsigned int size;		//size of memory
	int mode;				//memory mode
	void *mptr;				//pointer to memory
}MEM_STRUCT;


extern bool Memory_Read8Bits(MEM_STRUCT *ms,unsigned int address,unsigned char *data);
extern bool Memory_Close(MEM_STRUCT *ms);
extern unsigned int Memory_Dump(MEM_STRUCT *ms,unsigned int start,char *sptr);
extern bool Memory_Read16Bits(MEM_STRUCT *ms,unsigned int address,unsigned short *data);
extern bool Memory_Write32Bits(MEM_STRUCT *ms,unsigned int address,unsigned int data);
extern bool Memory_Read32Bits(MEM_STRUCT *ms,unsigned int address,unsigned int *data);
extern MEM_STRUCT *Memory_Init(int start, int size, bool mode);
extern int Memory_ReadFile(MEM_STRUCT *ms,char *filename,unsigned int start);

#endif
