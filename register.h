/*
Register header files
*/

#ifndef REGISTER_H
#define REGISTER_H

#define REG_LINKS	14
#define REG_PC   	15


#define REG_MAX_REG  16
#define REG_MAX_BNK  32

#define BL_LSL	0
#define BL_LSR	1
#define BL_ASR	2
#define BL_ROR	3

typedef struct
{
    unsigned N:1;                   //status
    unsigned Z:1;                   //Z status
    unsigned C:1;                   //C status
    unsigned V:1;                   //V status

    unsigned NU:20;                 //not used

    unsigned I:1;                   //I status
    unsigned F:1;                   //F status
    unsigned T:1;                   //T status
    unsigned mode:5;                //cpu mode
}REG_STATUS;

typedef struct
{
    unsigned flags:4;

    unsigned NU:20;

    unsigned I:1;                   //I status
    unsigned F:1;                   //F status
    unsigned T:1;                   //T status
    unsigned mode:5;                //cpu mode
}REG_SGF;


typedef union
{
    REG_STATUS cs;
    REG_SGF    cg;
    int        st;
}REG_PSR;


typedef struct _REG_ARRAY
{
	unsigned int r[REG_MAX_REG];
	REG_PSR psr;
}REG_ARRAY;

typedef struct _REG_STRUCT
{
	unsigned int rs[REG_MAX_REG];
	REG_PSR cpsr;
}REG_STRUCT;

extern int REG_Barrel(REG_STRUCT *rb, int index, int shift);
extern bool REG_C_Write(REG_STRUCT *r, bool f);
extern bool REG_C_Status(REG_STRUCT *r);
extern bool REG_Close(REG_STRUCT *r);
extern bool REG_Clr_PSR(REG_STRUCT *r);

extern bool REG_Reset(REG_STRUCT *r);
extern bool REG_LE_Status(REG_STRUCT *r);
extern bool REG_GT_Status(REG_STRUCT *r);
extern bool REG_LT_Status(REG_STRUCT *r);
extern bool REG_GE_Status(REG_STRUCT *r);
extern bool REG_LS_Status(REG_STRUCT *r);
extern bool REG_HI_Status(REG_STRUCT *r);
extern bool REG_V_Status(REG_STRUCT *r);
extern bool REG_N_Status(REG_STRUCT *r);
extern bool REG_Z_Status(REG_STRUCT *r);

extern bool REG_PC_Inc4(REG_STRUCT *r);
extern int REG_PCAdd(REG_STRUCT *r,int bank,int v);
extern int REG_Read_pc(REG_STRUCT *r);
extern REG_STRUCT * REG_Init(void);

extern int REG_Read(REG_STRUCT *r, int index);
extern bool REG_Write(REG_STRUCT *r,int index,int d);
extern bool REG_Write_Link(REG_STRUCT *r, unsigned int d);
extern bool REG_Write_pc(REG_STRUCT *r, unsigned int pc);

#endif
