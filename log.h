//*****************************************
#ifndef LOG_H
#define LOG_H

#define LOGFILE	"gl.log"	 	// all Log(); messages will be appended to this file
extern bool LogCreated;			// keeps track whether the log file is created or not
extern void Log (char *message);	// logs a message to LOGFILE
extern void LogErr (char *message);	// logs a message; execution is interrupted



#endif
