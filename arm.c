/*
ARM Execution
*/
#include <stdlib.h>
#include <stdbool.h>

#include "log.h"
#include "memory.h"
#include "register.h"
#include "arm.h"

enum
{
    PS_FETCH,           //Initial state
    PS_FDECODE,         //do another fetch and decode last fetch
    PS_FDEXEC,          //do another fetch and decode last fetch plus execute previous decode
    PS_EXCEP,
};


//structure for opcode decode
typedef struct
{
	unsigned int mask;
	unsigned int opcode;
	void (*opcodecall)(CPU_STRUCT *);
}OPCODE_TABLE;




void OP_SWP(CPU_STRUCT *cpu)
{
	if(cpu != NULL)
	{
		cpu->rb = cpu->rb;
	}
//		unsigned int dwtmp=REG(DECODEDINS.Rm);
//		instclock=4;

///		REG(DECODEDINS.Rd) = *(unsigned int *)(mem+REG(DECODEDINS.Rn));
///        *(unsigned int *)(mem+REG(DECODEDINS.Rn))=dwtmp;

//	if(DECODEDINS.Rd!=15)
//	{
//		PC+=4;
//		REG(15)+=4;
//	}
//	else
//	{
//		REG(15)&=0xfffffffc;
//		PC=REG(15);
//		REG(15)=PC+PC_AHEAD;
//	}
}

//****************************************************************
//Description:	Emulate the ARM Barrel shift
//Arguments:
//****************************************************************
int ARM_Barrel(CPU_STRUCT *cpu, int mode, int d )
{
	int retval = 0;
	int st;
	int sr;
	int sa;

//	int rm;

    if(cpu != NULL)
	{
		st = (mode >> 5) & 0x03;	//extract the shift type
		sa = (mode >> 7) & 0x1F;	//extract shift ammount
		sr = (mode >> 8) & 0x0F;	//extract shift register

		if((mode & 0x01) && 0x01)
		{
			switch(st)
			{
				case BL_LSL:	//Logical shift Left
					while(sa)
					{
						REG_C_Write(cpu->rb,d >> 31);
						d <<= 1;
						sa--;
					}
					break;

				case BL_LSR:	//Logical shift Right
					while(sa)
					{
						REG_C_Write(cpu->rb,d & 0x01);
						d >>= 1;
						sa--;
					}
					break;

				case BL_ASR:	//Arithmatic shift right
					while(sa)
					{
						sr = d & 0x80000000;
						REG_C_Write(cpu->rb,d & 0x01);
						d >>= 1;
						d |= sr;
						sa--;
					}
					break;

				case BL_ROR:	//Rotate Right
					while(sa)
					{
						sr = d << 31;
						REG_C_Write(cpu->rb,d & 0x01);
						d >>= 1;
						d |= sr;
						sa--;
					}
					break;

				default:		//error
					break;
			}
		}
		else
		{
			switch(st)
			{
				case BL_LSL:	//Logical shift Left
					while(sa)
					{
						REG_C_Write(cpu->rb,d >> 31);
						d <<= 1;
						sa--;
					}
					break;

				case BL_LSR:	//Logical shift Right
					while(sa)
					{
						REG_C_Write(cpu->rb,d & 0x01);
						d >>= 1;
						sa--;
					}
					break;

				case BL_ASR:	//Arithmatic shift right
					while(sa)
					{
						sr = d & 0x80000000;
						REG_C_Write(cpu->rb,d & 0x01);
						d >>= 1;
						d |= sr;
						sa--;
					}
					break;

				case BL_ROR:	//Rotate Right
					while(sa)
					{
						sr = d << 31;
						REG_C_Write(cpu->rb,d & 0x01);
						d >>= 1;
						d |= sr;
						sa--;
					}
					break;

				default:		//error
					break;
			}
        }
        retval = d;
	}
	return(retval);
}



//******************************************************************
//Descrition:	turn an integer into a 12 Bit Signed integer
//Arguments:	integer
//Return:		signed 12 bit integer
//******************************************************************
int ARM_Sign12bit(int val)
{
	int retval = 0;

	retval = (val & 0x00000800) << 28;
	retval = retval | val;

	return(retval);
}



//****************************************************************
//Description:	Load Immediate instruction
//Arguments:	Pointer to cpu structure
//*****************************************************************
void ARM_op_ldr_imm(CPU_STRUCT *cpu)
{
	int offset;
	int base;
//	int shift;
	int breg;
	int dest;
	unsigned int data;
	int code;
	unsigned char cdata;

	if(cpu != NULL)
	{
		code = (cpu->decode >> 20) & 0x3F;
		base = (cpu->decode >> 16) & 0x0F;
//		src = (cpu->decode >> 12) & 0x0F;
//		imm = (cpu->decode >> 25) & 0x01;



		switch(code)
		{
			case 0x01:	//load nwb word down post imm
				offset = cpu->decode & 0x00000FFF;
				breg = REG_Read(cpu->rb,base);
				Memory_Read32Bits(cpu->ms,breg,&data);
				REG_Write(cpu->rb,dest,data);
				breg = breg - offset;
				break;

			case 0x03:	//load wb word down post imm
				offset = cpu->decode & 0x00000FFF;
				breg = REG_Read(cpu->rb,base);
				Memory_Read32Bits(cpu->ms,breg,&data);
				REG_Write(cpu->rb,dest,data);
				breg = breg - offset;
				REG_Write(cpu->rb,base,breg);
				break;

			case 0x05:	//load nwb byte down post imm
				offset = cpu->decode & 0x00000FFF;
				breg = REG_Read(cpu->rb,base);
				Memory_Read8Bits(cpu->ms,breg,&cdata);
				data = cdata;
				REG_Write(cpu->rb,dest,data);
				breg = breg - offset;
				break;

			case 0x07:	//load wb byte down post imm
				offset = cpu->decode & 0x00000FFF;
				breg = REG_Read(cpu->rb,base);
				Memory_Read8Bits(cpu->ms,breg,&cdata);
				data = cdata;
				REG_Write(cpu->rb,dest,data);
				breg = breg - offset;
				REG_Write(cpu->rb,base,breg);
				break;

			case 0x09:	//load nwb word up post imm
				offset = cpu->decode & 0x00000FFF;
				breg = REG_Read(cpu->rb,base);
				Memory_Read32Bits(cpu->ms,breg,&data);
				REG_Write(cpu->rb,dest,data);
				breg = breg + offset;
				break;

			case 0x0B:	//load wb word up post imm
				offset = cpu->decode & 0x00000FFF;
				breg = REG_Read(cpu->rb,base);
				Memory_Read32Bits(cpu->ms,breg,&data);
				REG_Write(cpu->rb,dest,data);
				breg = breg + offset;
				REG_Write(cpu->rb,base,breg);
				break;

			case 0x0D:	//load nwb byte up post imm
				offset = cpu->decode & 0x00000FFF;
				breg = REG_Read(cpu->rb,base);
				Memory_Read8Bits(cpu->ms,breg,&cdata);
				data = cdata;
				REG_Write(cpu->rb,dest,data);
				breg = breg + offset;
				break;

			case 0x0F:	//load wb byte up post imm
				offset = cpu->decode & 0x00000FFF;
				breg = REG_Read(cpu->rb,base);
				Memory_Read8Bits(cpu->ms,breg,&cdata);
				data = cdata;
				REG_Write(cpu->rb,dest,data);
				breg = breg + offset;
				REG_Write(cpu->rb,base,breg);
				break;

			case 0x11:	//load nwb word down pre immm
				offset = cpu->decode & 0x00000FFF;
				breg = REG_Read(cpu->rb,base);
				breg = breg - offset;
				Memory_Read32Bits(cpu->ms,breg,&data);
				REG_Write(cpu->rb,dest,data);
				break;

			case 0x13:	//load wb word down pre imm
				offset = cpu->decode & 0x00000FFF;
				breg = REG_Read(cpu->rb,base);
				breg = breg - offset;
				Memory_Read32Bits(cpu->ms,breg,&data);
				REG_Write(cpu->rb,dest,data);
				REG_Write(cpu->rb,base,breg);
				break;

			case 0x15:	//load nwb byte down pre imm
				offset = cpu->decode & 0x00000FFF;
				breg = REG_Read(cpu->rb,base);
				breg = breg - offset;
				Memory_Read8Bits(cpu->ms,breg,&cdata);
				data = cdata;
				REG_Write(cpu->rb,dest,data);
				break;

			case 0x17:	//load wb byte down pre imm
				offset = cpu->decode & 0x00000FFF;
				breg = REG_Read(cpu->rb,base);
				breg = breg - offset;
				Memory_Read8Bits(cpu->ms,breg,&cdata);
				data = cdata;
				REG_Write(cpu->rb,dest,data);
				REG_Write(cpu->rb,base,breg);
				break;

			case 0x19:	//load nwb word up pre imm
				offset = cpu->decode & 0x00000FFF;
				breg = REG_Read(cpu->rb,base);
				breg = breg + offset;
				Memory_Read32Bits(cpu->ms,breg,&data);
				REG_Write(cpu->rb,dest,data);
				break;

			case 0x1B:	//load wb word up pre imm
				offset = cpu->decode & 0x00000FFF;
				breg = REG_Read(cpu->rb,base);
				breg = breg + offset;
				Memory_Read32Bits(cpu->ms,breg,&data);
				REG_Write(cpu->rb,dest,data);
				REG_Write(cpu->rb,base,breg);
				break;

			case 0x1D:	//load nwb byte up pre imm
				offset = cpu->decode & 0x00000FFF;
				breg = REG_Read(cpu->rb,base);
				breg = breg + offset;
				Memory_Read8Bits(cpu->ms,breg,&cdata);
				data = cdata;
				REG_Write(cpu->rb,dest,data);
				break;

			case 0x1F:	//load wb byte up pre imm
				offset = cpu->decode & 0x00000FFF;
				breg = REG_Read(cpu->rb,base);
				breg = breg + offset;
				Memory_Read8Bits(cpu->ms,breg,&cdata);
				data = cdata;
				REG_Write(cpu->rb,dest,data);
				REG_Write(cpu->rb,base,breg);
				break;

			case 0x21:	//shift nwb word down post imm
			default:
				break;
		}
	}
}

//***********************************************************
//Description:	Branch relative
//Arguments:	Pointer to CPU Structure
//***********************************************************
void ARM_op_bn(CPU_STRUCT *cpu)
{
	int pc;


	if(cpu != NULL)
	{
		pc = REG_Read_pc(cpu->rb);
		if(cpu->link)
		{
			REG_Write_Link(cpu->rb,pc);
		}
		pc = pc + (cpu->offset * 4);
		REG_Write_pc(cpu->rb,pc);
		cpu->pipestate = PS_FETCH;			//pre fetch needs to reset
	}
}

//**********************************************************
//Description: MOV instruction
//Arguments:    Pointer to cpu structure
//**********************************************************
void ARM_op_mov(CPU_STRUCT *cpu)
{
//	int offset;
	int base;
	int shift;
	//int breg;
	int dest;
	int rm;
	//unsigned int data;
//	int code;
	//unsigned char cdata;

	if(cpu != NULL)
	{
	    rm = cpu->decode & 0x0F;
        shift = (cpu->decode >> 4) & 0x7F;
		dest = (cpu->decode >> 12) & 0x0F;
		base = REG_Barrel(cpu->rb,rm,shift);
        REG_Write(cpu->rb,dest,base);
        if((cpu->decode & 0x0100000) && 0x0100000) //s bit 20
        {

        }
	}
}



//opcode lookup table
OPCODE_TABLE opcodetable[]=
{
	//swap
	{0x0FF00FF0,0x01000090,	&OP_SWP},   //0
	{0x0FF00FF0,0x01400F90,	&OP_SWP},					//&OP_SWPB},
	//state register accessing instructions
	{0x0FF00000,0x01000000,	&OP_SWP},					//&OP_MRS_CPSR},
	{0x0FF00000,0x01400000,	&OP_SWP},					//&OP_MRS_SPSR},
	{0x0FF000F0,0x01200000,	&OP_SWP},	//				//&OP_MSR_CPSR_RM},
	{0x0FF000F0,0x01600000,	&OP_SWP},	//5				//&OP_MSR_SPSR_RM},
	{0x0FF00000,0x03200000,	&OP_SWP},					//&OP_MSR_CPSR_IMM},
	{0x0FF00000,0x03600000,	&OP_SWP},					//&OP_MSR_SPSR_IMM},
	//multiply
	{0x0FE0F0F0,0x00000090,	&OP_SWP},					//&OP_MUL},
	{0x0FE000F0,0x00200090,	&OP_SWP},					//&OP_MLA},
	{0x0FE000F0,0x00800090,	&OP_SWP},	//10			//&OP_UMULL},
	{0x0FE000F0,0x00A00090,	&OP_SWP},					//&OP_UMLAL},
	{0x0FE000F0,0x00C00090,	&OP_SWP},					//&OP_SMULL},
	{0x0FE000F0,0x00E00090,	&OP_SWP},					//&OP_SMLAL},
	//misc
	{0x0FFF0FF0,0x016F0F10,	&OP_SWP},					//&OP_CLZ},
	//ALU instructions
	{0x0DE00000,0x00000000,	&OP_SWP},	//15			//&OP_AND},
	{0x0DE00000,0x00200000,	&OP_SWP},					//&OP_EOR},
	{0x0DE00000,0x00400000,	&OP_SWP},					//&OP_SUB},
	{0x0DE00000,0x00600000,	&OP_SWP},					//&OP_RSB},
	{0x0DE00000,0x00800000,	&OP_SWP},					//&OP_ADD},
	{0x0DE00000,0x00A00000,	&OP_SWP},	//20			//&OP_ADC},
	{0x0DE00000,0x00C00000,	&OP_SWP},					//&OP_SBC},
	{0x0DE00000,0x00E00000,	&OP_SWP},					//&OP_RSC},
	{0x0DE00000,0x01000000,	&OP_SWP},					//&OP_TST},
	{0x0DE00000,0x01200000,	&OP_SWP},					//&OP_TEQ},
	{0x0DE00000,0x01400000,	&OP_SWP},	//25			//&OP_CMP},
	{0x0DE00000,0x01600000,	&OP_SWP},					//&OP_CMN},
	{0x0DE00000,0x01800000, &OP_SWP},               	//&OP_ORR},
	{0x0DE00000,0x01A00000,&ARM_op_mov},
	{0x0DE00000,0x01C00000,	&OP_SWP},					//&OP_BIC},
	{0x0DE00000,0x01E00000,	&OP_SWP},	//30			//&OP_MVN},
	//LDR/STR w/o write-back w=0,p=1
	{0x0F700000,0x05100000,&ARM_op_ldr_imm},            //31
	{0x0F700000,0x05500000,	&OP_SWP},					//&OP_LDRB_IMM},
	{0x0F700000,0x05000000,	&OP_SWP},					//&OP_STR_IMM},
	{0x0F700000,0x05400000,	&OP_SWP},					//&OP_STRB_IMM},
	{0x0F700FF0,0x07100000,	&OP_SWP},	//35			//&OP_LDR_RM},
	{0x0F700FF0,0x07500000,	&OP_SWP},					//&OP_LDRB_RM},
	{0x0F700FF0,0x07000000,	&OP_SWP},					//&OP_STR_RM},
	{0x0F700FF0,0x07400000,	&OP_SWP},					//&OP_STRB_RM},
	{0x0F700010,0x07100000,	&OP_SWP},					//&OP_LDR_RM_SHIFT},
	{0x0F700010,0x07500000,	&OP_SWP},	//40			//&OP_LDRB_RM_SHIFT},
	{0x0F700010,0x07000000,	&OP_SWP},					//&OP_STR_RM_SHIFT},
	{0x0F700010,0x07400000,	&OP_SWP},					//&OP_STRB_RM_SHIFT},
	//LDR/STR pre-indexed w=1,p=1
	{0x0F700000,0x05300000,	&OP_SWP},					//&OP_LDR_IMM_PRE},
	{0x0F700000,0x05700000,	&OP_SWP},					//&OP_LDRB_IMM_PRE},
	{0x0F700000,0x05200000,	&OP_SWP},	//45			//&OP_STR_IMM_PRE},
	{0x0F700000,0x05600000,	&OP_SWP},					//&OP_STRB_IMM_PRE},
	{0x0F700FF0,0x07300000,	&OP_SWP},					//&OP_LDR_RM_PRE},
	{0x0F700FF0,0x07700000,	&OP_SWP},					//&OP_LDRB_RM_PRE},
	{0x0F700FF0,0x07200000,	&OP_SWP},					//&OP_STR_RM_PRE},
	{0x0F700FF0,0x07600000,	&OP_SWP},	//50			//&OP_STRB_RM_PRE},
	{0x0F700010,0x07300000,	&OP_SWP},					//&OP_LDR_RM_SHIFT_PRE},
	{0x0F700010,0x07700000,	&OP_SWP},					//&OP_LDRB_RM_SHIFT_PRE},
	{0x0F700010,0x07200000,	&OP_SWP},					//&OP_STR_RM_SHIFT_PRE},
	{0x0F700010,0x07600000,	&OP_SWP},					//&OP_STRB_RM_SHIFT_PRE},
	//LDR/STR post-indexed w=0,p=0
	{0x0F700000,0x04100000,	&OP_SWP},	//55			//&OP_LDR_IMM_POST},
	{0x0F700000,0x04500000,	&OP_SWP},					//&OP_LDRB_IMM_POST},
	{0x0F700000,0x04000000,	&OP_SWP},					//&OP_STR_IMM_POST},
	{0x0F700000,0x04400000,	&OP_SWP},					//&OP_STRB_IMM_POST},
	{0x0F700FF0,0x06100000,	&OP_SWP},					//&OP_LDR_RM_POST},
	{0x0F700FF0,0x06500000,	&OP_SWP},	//60			//&OP_LDRB_RM_POST},
	{0x0F700FF0,0x06000000,	&OP_SWP},					//&OP_STR_RM_POST},
	{0x0F700FF0,0x06400000,	&OP_SWP},					//&OP_STRB_RM_POST},
	{0x0F700010,0x06100000,	&OP_SWP},					//&OP_LDR_RM_SHIFT_POST},
	{0x0F700010,0x06500000,	&OP_SWP},					//&OP_LDRB_RM_SHIFT_POST},
	{0x0F700010,0x06000000,	&OP_SWP},	//65			//&OP_STR_RM_SHIFT_POST},
	{0x0F700010,0x06400000,	&OP_SWP},					//&OP_STRB_RM_SHIFT_POST},
	// LDRH/STRH
	{0x0F7000F0,0x015000B0,	&OP_SWP},					//&OP_LDRH_IMM},
	{0x0F7000F0,0x017000B0,	&OP_SWP},					//&OP_LDRH_IMM_PRE},
	{0x0F7000F0,0x005000B0,	&OP_SWP},					//&OP_LDRH_IMM_POST},
	{0x0F7000F0,0x014000B0,	&OP_SWP},	//70    		//&OP_STRH_IMM},
	{0x0F7000F0,0x016000B0,	&OP_SWP},					//&OP_STRH_IMM_PRE},
	{0x0F7000F0,0x004000B0,	&OP_SWP},					//&OP_STRH_IMM_POST},
	{0x0F7000F0,0x011000B0,	&OP_SWP},					//&OP_LDRH_RM},
	{0x0F7000F0,0x013000B0,	&OP_SWP},					//&OP_LDRH_RM_PRE},
	{0x0F7000F0,0x001000B0,	&OP_SWP},	//75			//&OP_LDRH_RM_POST},
	{0x0F7000F0,0x010000B0,	&OP_SWP},					//&OP_STRH_RM},
	{0x0F7000F0,0x012000B0,	&OP_SWP},					//&OP_STRH_RM_PRE},
	{0x0F7000F0,0x000000B0,	&OP_SWP},					//&OP_STRH_RM_POST},
	// LDRSB
	{0x0F7000F0,0x015000D0,	&OP_SWP},					//&OP_LDRSB_IMM},
	{0x0F7000F0,0x017000D0,	&OP_SWP},	//80			//&OP_LDRSB_IMM_PRE},
	{0x0F7000F0,0x005000D0,	&OP_SWP},					//&OP_LDRSB_IMM_POST},
	{0x0F7000F0,0x011000D0,	&OP_SWP},					//&OP_LDRSB_RM},
	{0x0F7000F0,0x013000D0,	&OP_SWP},					//&OP_LDRSB_RM_PRE},
	{0x0F7000F0,0x001000D0,	&OP_SWP},					//&OP_LDRSB_RM_POST},
	// LDRSH
	{0x0F7000F0,0x015000F0,	&OP_SWP},	//85			//&OP_LDRSH_IMM},
	{0x0F7000F0,0x017000F0,	&OP_SWP},					//&OP_LDRSH_IMM_PRE},
	{0x0F7000F0,0x005000F0,	&OP_SWP},					//&OP_LDRSH_IMM_POST},
	{0x0F7000F0,0x011000F0,	&OP_SWP},					//&OP_LDRSH_RM},
	{0x0F7000F0,0x013000F0,	&OP_SWP},					//&OP_LDRSH_RM_PRE},
	{0x0F7000F0,0x001000F0,	&OP_SWP},	//90			//&OP_LDRSH_RM_POST},
	//LDM
	{0x0FF00000,0x08900000,	&OP_SWP},					//&OP_LDM_INC_POST},
	{0x0FF00000,0x08100000,	&OP_SWP},					//&OP_LDM_DEC_POST},
	{0x0FF00000,0x09900000,	&OP_SWP},					//&OP_LDM_INC_PRE},
	{0x0FF00000,0x09100000,	&OP_SWP},					//&OP_LDM_DEC_PRE},
	{0x0FF00000,0x08B00000,	&OP_SWP},	//95			//&OP_LDM_INC_POST_WB},
	{0x0FF00000,0x08300000,	&OP_SWP},					//&OP_LDM_DEC_POST_WB},
	{0x0FF00000,0x09B00000,	&OP_SWP},					//&OP_LDM_INC_PRE_WB},
	{0x0FF00000,0x09300000,	&OP_SWP},					//&OP_LDM_DEC_PRE_WB},
	{0x0FF00000,0x08D00000,	&OP_SWP},					//&OP_LDM_INC_POST_S},
	{0x0FF00000,0x08500000,	&OP_SWP},	//100			//&OP_LDM_DEC_POST_S},
	{0x0FF00000,0x09D00000,	&OP_SWP},					//&OP_LDM_INC_PRE_S},
	{0x0FF00000,0x09500000,	&OP_SWP},					//&OP_LDM_DEC_PRE_S},
	//STM
	{0x0FF00000,0x08800000,	&OP_SWP},					//&OP_STM_INC_POST},
	{0x0FF00000,0x08000000,	&OP_SWP},					//&OP_STM_DEC_POST},
	{0x0FF00000,0x09800000,	&OP_SWP},	//105   		//&OP_STM_INC_PRE},
	{0x0FF00000,0x09000000,	&OP_SWP},					//&OP_STM_DEC_PRE},
	{0x0FF00000,0x08A00000,	&OP_SWP},					//&OP_STM_INC_POST_WB},
	{0x0FF00000,0x08200000,	&OP_SWP},					//&OP_STM_DEC_POST_WB},
	{0x0FF00000,0x09A00000,	&OP_SWP},					//&OP_STM_INC_PRE_WB},
	{0x0FF00000,0x09200000,	&OP_SWP},	//110			//&OP_STM_DEC_PRE_WB},
	{0x0FF00000,0x08C00000,	&OP_SWP},					//&OP_STM_INC_POST_S},
	{0x0FF00000,0x08400000,	&OP_SWP},					//&OP_STM_DEC_POST_S},
	{0x0FF00000,0x09C00000,	&OP_SWP},					//&OP_STM_INC_PRE_S},
	{0x0FF00000,0x09400000,	&OP_SWP},					//&OP_STM_DEC_PRE_S},
	//jump
	{0x0FFFFFF0,0x012FFF10,	&OP_SWP},	//115			//&OP_BX},
	{0x0FE00000,0x0FA00000,	&OP_SWP},					//&OP_BLX1},
	{0x0FFFFFF0,0x0FF00030,	&OP_SWP},					//&OP_BLX2},
	{0x0F000000,0x0A000000,&ARM_op_bn},
	{0x0F000000,0x0B000000,&ARM_op_bn},	//branch with link
	//special
	{0x0F000000,0x0F000000,	&OP_SWP},	//120			//&OP_SWI},
  //{0xFF000F0,	0x1200070,	&OP_BKPT},
	{0x0E000010,0x06000010,	&OP_SWP},					//&OP_UNDEF},
	{0,0,					&OP_SWP},					//&OP_NOP}
};








//***************************************************************************************
//Description:  ARM Reset do reset sequence
//Arguements:   Cpu Struture
//Returns:      True if completed
//              False if not
//**********************************************************************************
bool ARM_Reset(CPU_STRUCT *cpu)
{
	bool retval = false;

	if(cpu != NULL)
	{
 		cpu->pipestate = PS_FETCH;
		if(cpu->rb != NULL)
		{
			REG_Reset(cpu->rb);
			retval = true;
		}
	}
	return(retval);
}



//*****************************************************************************************
//Description: Fetch Instruction
//Arguments:    Pointer to cpu structure
//Return:       True for an error
//*****************************************************************************************
bool ARM_Fetch(CPU_STRUCT *cpu)
{
    bool retval = false;
    int pc;

    if(cpu != NULL)
    {
        pc = REG_Read_pc(cpu->rb);
        Memory_Read32Bits(cpu->ms,pc,&cpu->opcode);
		retval = true;
    }
    return(retval);
}


//****************************************************************************************
//Decription:   Decode the cpu instruction
//Arguments:    Pointer to cpu structure
//****************************************************************************************
int ARM_Decode(CPU_STRUCT *cpu)
{
	int retval = 0;
	int i;

	if(cpu != NULL)
	{
		for(i=0; opcodetable[i].mask; i++)
		{
			if((cpu->opcode & opcodetable[i].mask) == opcodetable[i].opcode)
			{
				cpu->opcodecall = opcodetable[i].opcodecall;
				cpu->decode = cpu->opcode;	//make sure we copy opcode
				cpu->cond = (unsigned char)(cpu->opcode >> 28) & 0x0f;
				cpu->rn = (unsigned char )(cpu->opcode >> 16) & 0x0f;
				cpu->rm = (unsigned char ) cpu->opcode & 0x0f;
				cpu->rd = (unsigned char )(cpu->opcode >> 12) & 0x0f;
				cpu->rs = (unsigned char )(cpu->opcode >> 8);
				cpu->offset = cpu->opcode & 0x00FFFFFF;
				cpu->link = (cpu->opcode >> 24) & 0x00000001;
				return(retval);
            }
        }
	}
//	cpu->opcodecall = &OP_NOP;
 	return(retval);
}


//*******************************************************************************************
//Description:  Do a one Pipe cycle
//Arguements:   Pointer to CPU Structure
//Returns:      True if one instruction has been completed
//*****************************************************************************************
bool ARM_Cycle(CPU_STRUCT *cpu)
{
    bool retval = false;

    if(cpu != NULL)
    {
        switch(cpu->pipestate)
        {
            case PS_FDEXEC:
                ARM_Execute(cpu);
				if(cpu->pipestate != PS_FDEXEC)
				{
					break;
				}
                retval = true;

            case PS_FDECODE:
                ARM_Decode(cpu);

            case PS_FETCH:
                if(ARM_Fetch(cpu))
                {
                    if(cpu->pipestate != PS_FDEXEC) cpu->pipestate++;

                    REG_PC_Inc4(cpu->rb);
                }
                else
                {
                    cpu->pipestate = PS_EXCEP;
                    retval = false;
                }
                break;

            case PS_EXCEP:  //
                //we need to do some kind exception
                break;
        }
    }
	return(retval);
}


//******************************************************************
//Description:  do an executon of opcode
//Argument:		Pointer to CPU Structure
//******************************************************************
void ARM_Execute(CPU_STRUCT *cpu)
{
	switch(cpu->cond)
	{
		case COND_EQ:
			if(REG_Z_Status(cpu->rb))
			{
				cpu->opcodecall(cpu);
			}
			else
			{
				REG_PC_Inc4(cpu->rb);
			}
		    break;

        case COND_NE:
            if(!REG_Z_Status(cpu->rb))
            {
				cpu->opcodecall(cpu);
            }
            else
            {
                REG_PC_Inc4(cpu->rb);
            }
			break;

        case COND_CS:	//carry set
            if(REG_C_Status(cpu->rb))
            {
                cpu->opcodecall(cpu);
            }
            else
            {
                REG_PC_Inc4(cpu->rb);
            }
			break;

        case COND_CC:
            if(!REG_C_Status(cpu->rb))
            {
                cpu->opcodecall(cpu);
            }
            else
            {
                REG_PC_Inc4(cpu->rb);
            }
			break;

        case COND_MI:
            if(REG_N_Status(cpu->rb))
            {
                cpu->opcodecall(cpu);
            }
            else
            {
                REG_PC_Inc4(cpu->rb);
            }
			break;

        case COND_PL:
            if(!REG_N_Status(cpu->rb))
            {
                cpu->opcodecall(cpu);
            }
            else
            {
            	REG_PC_Inc4(cpu->rb);
            }
			break;

        case COND_VS:
            if(REG_V_Status(cpu->rb))
            {
                cpu->opcodecall(cpu);
            }
            else
            {
				REG_PC_Inc4(cpu->rb);
            }
			break;

        case COND_VC:
            if(!REG_V_Status(cpu->rb))
            {
                cpu->opcodecall(cpu);
            }
            else
            {
                REG_PC_Inc4(cpu->rb);
            }
			break;

        case COND_HI:
            if(REG_HI_Status(cpu->rb))
            {
                cpu->opcodecall(cpu);
            }
            else
            {
                REG_PC_Inc4(cpu->rb);
            }
			break;

        case COND_LS:
            if(REG_LS_Status(cpu->rb))
            {
                cpu->opcodecall(cpu);
            }
            else
            {
                REG_PC_Inc4(cpu->rb);
            }
		    break;

        case COND_GE:
            if(REG_GE_Status(cpu->rb))
            {
                cpu->opcodecall(cpu);
            }
            else
            {
                REG_PC_Inc4(cpu->rb);
            }
		    break;

        case COND_LT:
            if(REG_LT_Status(cpu->rb))
            {
                cpu->opcodecall(cpu);
            }
            else
            {
                REG_PC_Inc4(cpu->rb);
            }
            break;

        case COND_GT:
            if(REG_GT_Status(cpu->rb))
            {
                cpu->opcodecall(cpu);
            }
            else
            {
                REG_PC_Inc4(cpu->rb);
            }
		    break;

        case COND_LE:
            if(REG_LE_Status(cpu->rb))
            {
                cpu->opcodecall(cpu);
            }
            else
            {
				REG_PC_Inc4(cpu->rb);
            }
		    break;

        default:
            cpu->opcodecall(cpu);
            break;
	}
	return;
}



//******************************************************************************
//Description:	Do the thing the initilise this routine
//******************************************************************************
CPU_STRUCT * ARM_Init(void)
{
	CPU_STRUCT *cpu = NULL;

	//initialize emulation


	if((cpu = malloc(sizeof(CPU_STRUCT))) != NULL)
	{
	    cpu->pipestate = PS_FETCH;
	    cpu->rb = REG_Init();
		if(cpu->rb == NULL)
		{
			LogErr("Cannot Allocate Register memory");
		}
		cpu->ms = Memory_Init(0,4096,false);
		if(cpu->ms == NULL)
		{
			LogErr("Cannot Allocate Memory Structure");
		}
	}

	return(cpu);
}


//**********************************************************************
//Description:  Destroy the arm structure
//Arguments:	Pointer to structure
//return:		true or false according to wheather closing failed
//**********************************************************************
bool ARM_Close(CPU_STRUCT *cpu)
{
	bool retval = false;

	if(cpu != NULL)
 	{
		REG_Close(cpu->rb);     //free the registers
        cpu->rb = NULL;         //I feel like a NULL
        Memory_Close(cpu->ms);
        cpu->ms = NULL;
        free(cpu);              //be free cpu
        cpu = NULL;             //be NULL cpu
		retval = true;
    }
	return(retval);
}


