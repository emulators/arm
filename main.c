#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <conio.h>


#include "memory.h"
#include "register.h"
#include "arm.h"


CPU_STRUCT *cpu;

int main(int argc, char *argv[])
{
	bool ex = false;
	char c;


	cpu = ARM_Init();
	if(cpu == NULL) return 0;

	ARM_Reset(cpu);
	Memory_ReadFile(cpu->ms,"ezcom_1.0.2.bin",0);

// 	system("PAUSE");
	while(!ex)
	{
		if(kbhit())
		{
			c = getch();
			if(c == 27) ex = true;
			if(c == 32) ARM_Cycle(cpu);
		}
//		sleep(0);
	}


	ARM_Close(cpu);
	cpu = NULL;

	return 0;
}
