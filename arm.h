/*
ARM Header File
*/

#ifndef ARM_H
#define ARM_H

/* Require Resister header */
//#include "memory.h"
//#include "register.h"



typedef struct
{
    unsigned N:1;                   //status
    unsigned Z:1;                   //Z status
    unsigned C:1;                   //C status
    unsigned V:1;                   //V status

    unsigned NU:20;                 //not used

    unsigned I:1;                   //I status
    unsigned F:1;                   //F status
    unsigned T:1;                   //T status
    unsigned mode:5;                //cpu mode
}CPU_STATUS;


typedef struct
{
    unsigned flags:4;

    unsigned NU:20;

    unsigned I:1;                   //I status
    unsigned F:1;                   //F status
    unsigned T:1;                   //T status
    unsigned mode:5;                //cpu mode
}CPU_SGF;



typedef union
{
    CPU_STATUS cs;
    CPU_SGF    cg;
    int        st;
}CPU_PSR;

typedef struct _CPU_STRUCT
{
    int pipestate;                      //Pipeline state
//    CPU_PSR cpsr;
    CPU_PSR spsr;
    unsigned int opcode;				//fetch puts opcode here
	unsigned int decode;				//decode pushes opcode to here
    int exception;
    REG_STRUCT *rb;
	MEM_STRUCT *ms;
    unsigned char rd;
    unsigned char rn;
    unsigned char rm;
    unsigned char rs;
    unsigned char cond;
	int offset;
	bool link;
    void (*opcodecall)(struct _CPU_STRUCT *);
}CPU_STRUCT;

//constants for conditions
#define COND_MASK 0xF0000000
#define COND_EQ 0x00
#define COND_NE 0x01
#define COND_CS 0x02
#define COND_CC 0x03
#define COND_MI 0x04
#define COND_PL 0x05
#define COND_VS 0x06
#define COND_VC 0x07
#define COND_HI 0x08
#define COND_LS 0x09
#define COND_GE 0x0A
#define COND_LT 0x0B
#define COND_GT 0x0C
#define COND_LE 0x0D
#define COND_AL 0x0E

extern bool ARM_Reset(CPU_STRUCT *cpu);
extern CPU_STRUCT *ARM_Init(void);
extern bool ARM_Close(CPU_STRUCT *cpu);
extern void ARM_Execute(CPU_STRUCT *cpu);
extern bool ARM_Cycle(CPU_STRUCT *cpu);

#endif
